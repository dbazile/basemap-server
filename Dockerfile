FROM docker.io/alpine:3 AS builder

RUN apk add gdal gdal-tools python3 py3-gdal
WORKDIR /work
COPY data/ .
COPY scripts/mbtilegen .
RUN ./mbtilegen \
	-v \
	-o main.mbtiles \
	place:country.json \
	place:province.json \
	place:urban.json \
	place:populated.json \
	place_label:country_labels.json \
	place_label:province_labels.json \
	water:ocean.json \
	water:lake.json \
	water:river.json


FROM docker.io/maptiler/tileserver-gl-light:v4.6.5 AS runtime
USER root
WORKDIR /srv
COPY --from=builder /work/main.mbtiles /srv/
ADD fonts/Muli/muli-pbf.tar.gz /srv/fonts
COPY styles /srv/styles
COPY config.json /srv/config.json
RUN chown -R root.root /srv
RUN ln -s /usr/src/app/docker-entrypoint.sh /usr/bin/tileserver
USER node

ENV PORT=3000

CMD ["tileserver", "--port", "$PORT"]
