# basemap-server

> Batteries-included visualization basemap server using
> [Natural Earth Data](https://www.naturalearthdata.com) as source data,
> serialized by [GDAL](https://gdal.org/drivers/raster/mbtiles.html)
> as [Mapbox Vector Tiles](https://docs.mapbox.com/data/tilesets/guides/vector-tiles-standards/),
> served by [tileserver-gl](https://maptiler-tileserver.readthedocs.io/en/latest/index.html)
> and styled by hand.


## Usage

### Podman

```bash
podman build -t basemap-server .

podman run \
    --rm \
    -it  \
    -p 8080:8080 \
    --name 'bm' \
    -v "$PWD/styles:/data/styles:ro,z" \
    -v "$PWD/config.json:/data/config.json:ro,z" \
    'basemap-server'

open http://localhost:8080
```


## Extending

```bash
# Fetch and extract NE data into data/ directory
./scripts/fetch

# Composes the extracted NE data from data/ into an .mbtiles file
./scripts/compose
```


### Generate mbtiles file

```bash
./scripts/compile_data
```


## Themes

### `/styles/black/style.json`

![black](styles/black/screenshot.png)


### `/styles/sepia/style.json`

![sepia](styles/sepia/screenshot.png)


### `/styles/silver/style.json`

![silver](styles/silver/screenshot.png)
